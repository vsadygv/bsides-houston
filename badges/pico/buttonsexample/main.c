#include <stdio.h>
#include "pico/stdlib.h"
#include "hardware/gpio.h"
#include "pico/binary_info.h"

int main(){
	const uint LED_PIN = PICO_DEFAULT_LED_PIN;
	const int n = 16;
	const int pin0 = 28;  
	const int pin1 = 26;
	const int pin2 = 21;
	const int pin3 = 16;
	const int pin4 = 27;
	const int pin5 = 20;
	const int pin6 = 17;
	const int pin7 = 22;
	const int pin8 = 19;
	const int pin9 = 18;
	gpio_init(pin0);
	gpio_init(pin1);
	gpio_init(pin2);
	gpio_init(pin3);
	gpio_init(pin4);
	gpio_init(pin5);
	gpio_init(pin6);
	gpio_init(pin7);
	gpio_init(pin8);
	gpio_init(pin9);
	gpio_init(LED_PIN);
	gpio_set_dir(LED_PIN, GPIO_OUT);
	gpio_set_dir(pin0,GPIO_IN);
	gpio_set_dir(pin1,GPIO_IN);
	gpio_set_dir(pin2,GPIO_IN);
	gpio_set_dir(pin3,GPIO_IN);
	gpio_set_dir(pin4,GPIO_IN);
	gpio_set_dir(pin5,GPIO_IN);
	gpio_set_dir(pin6,GPIO_IN);
	gpio_set_dir(pin7,GPIO_IN);
	gpio_set_dir(pin8,GPIO_IN);
	gpio_set_dir(pin9,GPIO_IN);
	int input[16] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
	int output[16] = {7,4,9,2,2,5,8,1,7,1,3,7,2,6,0,1};
	//int output[16] = {0,1,2,3,4,5,6,7,8,0,1,2,3,4,5,6};
	//int input[16] = {0,1,2,3,4,5,6,7,8,0,1,2,3,4,5,6};
		
	int temp = 0;
	int pressed = -1;
	bool added = false;
	bool passed = false;
	while(true){
		if(pressed != -1){
			gpio_put(LED_PIN,1);
			if(!added){
				for(int i=0;i<n-1;i++){
        			input[i]=input[i+1];
    			}
				input[n-1] = temp;
				//printf("%d ", input[i]);
				printf("%d ", temp);

				added = true;
			}	
			if(!gpio_get(pressed)){
				pressed = -1;
				added = false;
				gpio_put(LED_PIN,0);
			} 
		}
		else{	
			if(gpio_get(pin9)){
				temp = 9;
				pressed = pin9;
			}
			if(gpio_get(pin8)){
				temp = 8;
				pressed = pin8;
			}
			if(gpio_get(pin7)){
				temp = 7;
				pressed = pin7;
			}
			if(gpio_get(pin6)){
				temp = 6;
				pressed = pin6;
			}
			if(gpio_get(pin5)){
				temp = 5;
				pressed = pin5;
			}
			if(gpio_get(pin4)){
				temp = 4;
				pressed = pin4;
			}
			if(gpio_get(pin3)){
				temp = 3;
				pressed = pin3;
			}	
			if(gpio_get(pin2)){
				temp = 2;
				pressed = pin2;
			}
			if(gpio_get(pin1)){
				temp = 1;
				pressed = pin1;
			}
			if(gpio_get(pin0)){
				temp = 0;
				pressed = pin0;
			}
		}
		sleep_ms(1);
		passed = true;
		for (int i = 0; i < n-1; i++)
        	if (input[i] != output[i])
            	passed =  false;
 		if(passed){	
			gpio_put(LED_PIN,1);
		}else {
			gpio_put(LED_PIN,0);
		}
	}





}
